from path import Path
import numpy as np
import pandas as pd
import click


def extract_functions(filename):
    df = pd.read_csv(filename, sep="\t", names=["gene_name", "fcn"])
    df["fcn"] = df.fcn.replace("", "Unknown")
    return df.drop_duplicate().set_index("gene_name")


def extract_genes_from_CDS(
    filename, *, length_pc_start: float = 5, length_pc_stop: float = 85
):

    df = pd.read_csv(
        filename,
        sep="\t",
        usecols=[1, 2, 3, 5],
        index_col="gene_name",
        names=["start", "end", "gene_name", "strand"],
    ).sort_values(by="start")

    df["strand"] = df["strand"].replace({"+": +1, "-": -1, r"[^+-]": 0}).astype(int)
    df["gene_length"] = df.end - df.start
    df = df.drop(df[df.strand == 0].index)

    df["reduced_length"] = df.gene_length * (length_pc_stop - length_pc_start) / 100
    df["mod_left"] = np.where(
        df.strand == 1,
        df.start + df.gene_length * length_pc_start / 100,
        df.start + df.gene_length * (100 - length_pc_stop) / 100,
    )
    df["mod_right"] = np.where(
        df.strand == 1,
        df.end - 1 - df.gene_length * (100 - length_pc_stop) / 100,
        df.end - 1 - df.gene_length * length_pc_start / 100,
    )
    df["density"] = 1 / df.reduced_length

    return df


def extract_interval_from_readsfile(filename):
    df = pd.read_csv(
        filename, sep="\t", usecols=[3, 7, 9], names=["gene_name", "pos", "score"]
    )
    return df


def interval_genes_function_to_tradis_df(interval_df, gene_df, function_df=None):
    if function_df is None:
        function_df = pd.DataFrame(columns=["fcn"])
    gene_df = gene_df.merge(function_df, how="left", left_index=True, right_index=True)
    gene_df = gene_df.fillna({"fcn": gene_df.index.to_series()})

    extended_intersect = interval_df.merge(
        gene_df, left_on="gene_name", right_index=True
    )
    is_good_reads = ((extended_intersect.pos - 5) >= extended_intersect.mod_left) & (
        (extended_intersect.pos + 5) <= extended_intersect.mod_right
    )
    good_intersect = extended_intersect[is_good_reads]
    tradis_df = gene_df[["start", "end", "gene_length", "strand", "fcn"]].merge(
        pd.concat(
        [
            good_intersect.groupby("gene_name").score.sum().rename("read_count"),
            good_intersect.groupby("gene_name").score.size().rename("ins_count"),
            good_intersect.groupby("gene_name").density.sum().rename("ins_index"),
        ],
        axis=1,
        sort=True,
    ), how="left", left_index=True, right_index=True).rename_axis(index="gene_name")
    tradis_df = tradis_df.fillna(0).astype(
        {"read_count": "Int64", "ins_count": "Int64"}
    )
    # tradis_df["density_score"] = tradis_df.read_count * tradis_df.ins_index
    new_index = "gene_" + pd.Index(np.arange(1, len(tradis_df) + 1), dtype=str)
    tradis_df = (
        tradis_df.reset_index().set_index(new_index).rename_axis(index="locus_tag")
    )
    tradis_df["ncrna"] = 0
    tradis_df = tradis_df[
        [
            "gene_name",
            "ncrna",
            "start",
            "end",
            "strand",
            "read_count",
            "ins_index",
            "gene_length",
            "ins_count",
            "fcn",
        ]
    ]
    return tradis_df


def build_tradis_df(
    CDS_bedfile: Path,
    reads_bedfile: Path,
    gene_function_file: Path = None,
    length_pc_start: float = 5,
    length_pc_stop: float = 85,
):
    genes = extract_genes_from_CDS(
        CDS_bedfile, length_pc_start=length_pc_start, length_pc_stop=length_pc_stop
    )
    intervals = extract_interval_from_readsfile(reads_bedfile)
    gene_function_df = (
        gene_function_file
        if gene_function_file is None
        else extract_functions(gene_function_file)
    )
    tradis_df = interval_genes_function_to_tradis_df(intervals, genes, gene_function_df)
    return tradis_df


@click.command()
@click.argument("cds_bedfile", type=click.File())
@click.argument("reads_bedfile", type=click.File())
@click.option("--gene_function_file", type=click.File(), default=None)
@click.option("--length_pc_start", type=float, default=5)
@click.option("--length_pc_stop", type=float, default=85)
def cli(
    cds_bedfile,
    reads_bedfile,
    gene_function_file,
    length_pc_start: float = 5,
    length_pc_stop: float = 85,
):
    tradis_df = build_tradis_df(
        CDS_bedfile=cds_bedfile,
        reads_bedfile=reads_bedfile,
        gene_function_file=gene_function_file,
        length_pc_start=length_pc_start,
        length_pc_stop=length_pc_stop,
    )
    click.echo(tradis_df.to_csv(sep="\t"))


if __name__ == "__main__":
    cli()
