from path import Path
from Bio import SeqIO
from typing import Sequence
import pandas as pd
import numpy as np
import itertools as it


def process_sample(df, reads_dir: Path):
    df["genomes"] = df.genome.apply(lambda x: x.split("-") if isinstance(x, str) else [])
    df["plasmids"] = df.apply(lambda row: [plasmid for plasmid in (row.plasmid1, row.plasmid2) if isinstance(plasmid, str)], axis=1)
    df["reads"] = [
        Path(reads_dir).files(f"{sample}*.fastq*")[0] for sample in df.sample_name
    ]
    df["reads_name"] = [reads.basename().split(".")[0] for reads in df.reads]
    df = df[["sample_name", "reads", "genomes", "plasmids", "reads_name"]]
    df = df.drop_duplicates("sample_name").set_index("sample_name")
    df = df.reindex(sorted(df.index, key=lambda x: (int(x[:-1]), x[-1])))
    return df


class HDTM_Runs:
    def __init__(self, run_filename: Path, reads_dir: Path):
        self.raw_samples = pd.read_csv(run_filename, index_col=0).drop_duplicates(
            subset=["sample_name"]
        )
        self.samples = process_sample(self.raw_samples, reads_dir)

    def __getitem__(self, key):
        return self.samples.loc[key]

    @property
    def genomes(self):
        return set(it.chain(*self.samples.genomes))

    @property
    def plasmids(self):
        return set(it.chain(*self.samples.plasmids))


def build_chromsizes(genomes, chromsizes_output):
    lines = []
    for filename in genomes:
        try:
            seq = SeqIO.read(filename, "gb").seq
            chr_name = Path(filename).basename().stripext()
            size = len(seq)
            lines.append(f"{chr_name}\t{size}")
        except Exception:
            print(f"Unable to process file {filename}")

    with open(chromsizes_output, "w") as f:
        f.write("\n".join(lines))
