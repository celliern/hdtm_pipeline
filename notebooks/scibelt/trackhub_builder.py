from dataclasses import dataclass, field
import pandas as pd
import trackhub
import pendulum
from path import Path, tempfile
from typing import Optional, Iterable
import plumbum
from Bio import SeqIO
from matplotlib import colors


fa2bit = plumbum.local["faToTwoBit"]


def iter_on_fasta(fasta_file):
    with open(fasta_file) as f:
        yield from f.readlines()


def concat_fa(fasta_files, fasta_out):
    with open(fasta_out, "w") as f:
        for fasta_file in fasta_files:
            f.writelines(iter_on_fasta(fasta_file))
    return fasta_out


def c_to_trackhub_rgb(c):
    return ','.join(tuple(map(lambda x: str(int(x * 255)), colors.to_rgb(c))))


@dataclass
class AssemblyHub:
    hub_name: str
    ref_fa: Iterable[str]
    email: str
    genome: Optional[str] = "genome_refences"
    organism: Optional[str] = "various"
    desc: Optional[str] = None
    ref_2bit: Optional[str] = field(
        default_factory=tempfile.NamedTemporaryFile, init=False
    )

    def __post_init__(self):
        self._groups = {}
        self._init_hub()
        self._init_genomefile()
        self._init_parent_group()
        self._init_trackdb()

    def _init_hub(self):
        self.hub = trackhub.Hub(
            self.hub_name,
            short_label=self.hub_name,
            long_label=f"{self.hub_name.upper()} pipeline - {pendulum.now().to_cookie_string()}",
            email=self.email,
        )

    @property
    def default_pos(self):
        record = SeqIO.read(self.ref_fa[0], format="fasta")
        return f"{record.id}:1-{len(record.seq)}"

    @property
    def all_default_pos(self):
        for fa in self.ref_fa:
            record = SeqIO.read(fa, format="fasta")
            yield f"{record.id}:1-{len(record.seq)}"

    def _build_ref_2bits(self, ref2bit):
        with tempfile.NamedTemporaryFile("w") as catfa:
            concat_fa(self.ref_fa, catfa.name)
            fa2bit(catfa.name, ref2bit.name)

    def _init_genomefile(self):
        self._build_ref_2bits(self.ref_2bit)
        self.genome = trackhub.Assembly(
            genome=self.genome,
            organism=self.organism,
            twobit_file=self.ref_2bit.name,
            defaultPos=self.default_pos,
            orderKey=4800,
        )
        self.genomes_file = trackhub.GenomesFile()
        self.hub.add_genomes_file(self.genomes_file)
        self.genomes_file.add_genome(self.genome)

    def _init_parent_group(self):
        self.parent_group = trackhub.groups.GroupDefinition(
            "parent group", label="Container", priority=1, default_is_closed=False
        )
        self.groups_file = trackhub.groups.GroupsFile([self.parent_group])
        self.genome.add_groups(self.groups_file)

    def _init_trackdb(self):
        self.trackdb = trackhub.TrackDb()
        self.genome.add_trackdb(self.trackdb)

    def add_group(self, name, label, priority="auto"):
        if priority == "auto":
            priority = len(self.groups) + 2
        group = trackhub.groups.GroupDefinition(
            name, label=label, priority=priority,
            default_is_closed=False
        )
        self.genome.groups.groups.append(group)
        # groups_file = trackhub.groups.GroupsFile([group])
        # self.genome.add_groups(groups_file)
        self._groups[name] = group
        
    
    def add_track(self, name, track, tracktype, visibility="full", color="black", auto_scale=True, group=None):
        color_rgb = str(c_to_trackhub_rgb(color))
        track = trackhub.Track(
            name=name,
            source=track,
            visibility=visibility,
            color=color_rgb,
            autoScale="on" if auto_scale else "off",
            tracktype=tracktype,
        )
        if group is None:
            track.add_params(group=self.parent_group.name)
        elif group in self._groups:
            track.add_params(group=group)
        else:
            raise ValueError(f"Group {group} have to be added with `add_group` before being used!")
        self.trackdb.add_tracks(track)

    def upload(self, host, rdir):
        trackhub.upload.upload_hub(
            hub=self.hub, host=host, remote_dir=rdir, rsync_options="--delete -rvL"
        )

    def __del__(self):
        self.ref_2bit.close()
