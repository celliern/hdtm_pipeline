import plumbum
from path import Path, tempdir
import os
import pandas as pd
from loguru import logger
import pysam
from Bio import SeqIO
import numpy as np


def create_bedfile_from_insertion_samfile(
    sample_name: str, input_sam_file: Path, output_bed_file: Path = None
):
    def parse_read(read, sample_name):
        ref_pos = int(read.to_dict()["ref_pos"])
        if read.mapping_quality < 30:
            return
        if read.flag == 0:
            start = ref_pos + 3
            end = ref_pos + 4
            strand = "+"
        elif read.flag == 16:
            start = ref_pos + 44
            end = ref_pos + 45
            strand = "-"
        else:
            return
        return dict(
            sample_name=sample_name,
            start=start,
            end=end,
            label="i",
            score=999,
            strand=strand,
        )

    def gen_parsed_reads(sample_name, samfile):
        for read in samfile:
            parsed_read = parse_read(read, sample_name)
            if parsed_read:
                yield parsed_read

    map_ = pd.DataFrame(
        columns=["sample_name", "start", "end", "label", "score", "strand"]
    )
    with pysam.AlignmentFile(input_sam_file) as samfile:
        parsed_reads = list(gen_parsed_reads(sample_name, samfile))
        if parsed_reads:
            map_ = map_.append(parsed_reads, ignore_index=True)
    map_ = map_.drop_duplicates().sort_values(by=["start", "strand"])
    if output_bed_file is not None and output_bed_file != "-":
        map_.to_csv(output_bed_file, sep="\t", header=False, index=False)
    elif output_bed_file == "-":
        return map_.to_csv(sep="\t", header=False, index=False)
    else:
        return map_

def fasta_to_fake_fastq(input_fasta: Path, output_fastq: Path, read_len: int=50):
    logger.info(f"convert fake fasta {input_fasta} to fastq {output_fastq}")

    def convert_to_perfect_fastq(record):
        # phred_quality 40 == fastq "I" score
        record.letter_annotations["phred_quality"] = [40] * read_len
        return record

    records = SeqIO.parse(input_fasta, "fasta")
    with open(output_fastq, "w") as f:
        for record in records:
            record = convert_to_perfect_fastq(record)
            SeqIO.write(record, f, "fastq")

def build_simulated_bed(ref_fa: Path, read_len: int=50) -> pd.DataFrame():
    ref_name = Path(ref_fa).basename().stripext()
    record = SeqIO.read(ref_fa, "fasta")
    seq_len = len(record.seq)
    starts = np.arange(1, seq_len - read_len + 1)
    ends = starts + read_len
    pos_df = pd.DataFrame(
        data=dict(ref_name=ref_name, start=starts, end=ends, score=999, i="i", strand="+")
    )
    neg_df = pos_df.assign(strand="-")
    df = pd.concat([pos_df, neg_df]).sort_values(["start", "strand"])
    return df

def build_fake_chromsize(ref_fa, out_file):
    ref_name = Path(ref_fa).basename().stripext()
    record = SeqIO.read(ref_fa, "fasta")
    seq_len = len(record.seq)
    with open(out_file, "w") as f:
        print(ref_name, seq_len, sep="\t", file=f)