import pandas as pd
from Bio import SeqIO
from path import Path
from functools import partial
from warnings import warn

write_bed = partial(pd.DataFrame.to_csv, sep="\t", index=False, header=False)

def extract_label_from_feat(feat):
    for key in ["label", "gene", "note"]:
        try:
            return feat.qualifiers[key][0]
        except KeyError:
            continue
    raise KeyError(f"keys 'label' and 'gene' are not in the qualifiers. available keys: {', '.join(feat.qualifiers.keys())}")
            

def featCDS_to_bed(organism_name, features):
    for feat in filter(lambda feat: feat.type=="CDS", features):
        try:
            yield dict(organism_name=organism_name,
                       start=feat.location.start, end=feat.location.end,
                       gene_id=extract_label_from_feat(feat),
                       unamed=0, strand=feat.location.strand)
        except KeyError:
            warn(f"Skip feature {feat}, not able to give a label.")

def build_CDS(fasta_file):
    fasta_file = Path(fasta_file)
    organism_name = fasta_file.basename().stripext()
    records = SeqIO.read(fasta_file, format="genbank")
    df = pd.DataFrame(featCDS_to_bed(organism_name, records.features)).replace({"strand": {1: "+", -1: "-"}})
    return df