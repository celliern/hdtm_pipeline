---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.3.0
  kernelspec:
    display_name: Python [conda env:hdtm]
    language: python
    name: conda-env-hdtm-py
---

```python
from path import Path
import itertools as it
import papermill as pm
from scibelt.pre_process import HDTM_Runs
```

```python
INPUTS_DIR = Path("../data/inputs/")
INTERIM_DIR = Path("../data/interim/")
OUTPUTS_DIR = Path("../data/outputs/")
```

```python
runs = HDTM_Runs(INPUTS_DIR / "runs.csv", INPUTS_DIR / "reads")
```

```python
for i, plasmid in enumerate(runs.plasmids, 1):
    pm.execute_notebook(
        "01.00-sim_pipeline_template.ipynb",
        f"01.00-sim_pipeline_notebooks/{i:02d}-sim_pipeline_{plasmid}.ipynb",
        parameters=dict(plasmid=plasmid, overwrite=False, CDS_genes_whitelist=["Vcrx001"]),
        kernel_name="hdtm"
    )
```

```python
for i, sample_name in enumerate(runs.samples.index, 1):
    pm.execute_notebook(
        "01.01-hdtm_pipeline_template.ipynb",
        f"01.01-hdtm_pipeline_notebooks/{i:02d}-hdtm_pipeline_{sample_name}.ipynb",
        parameters=dict(sample_name=sample_name, overwrite=False),
        kernel_name="hdtm"
    )
```

```python

```
