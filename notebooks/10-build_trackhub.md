---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.3.0
  kernelspec:
    display_name: Python [conda env:hdtm]
    language: python
    name: conda-env-hdtm-py
---

```python
from path import Path
from itertools import chain
import trackhub
from scibelt.trackhub_builder import AssemblyHub
from scibelt.pre_process import HDTM_Runs
```

```python
INPUTS_DIR = Path("../data/inputs/")
INTERIM_DIR = Path("../data/interim/")
OUTPUTS_DIR = Path("../data/outputs/")
```

```python
EMAIL = "audrey.bioteau@gmail.com"
```

```python
runs = HDTM_Runs(INPUTS_DIR / "runs.csv", INPUTS_DIR / "reads/")
```

```python
all_fasta = [*[INTERIM_DIR / f"genomes/{genome}.fa" for genome in runs.genomes],
             *[INTERIM_DIR/ f"plasmids/{plasmid}.fa" for plasmid in runs.plasmids]]
hub = AssemblyHub("hdtm", all_fasta, EMAIL)
```

```python
hub.add_group("sim", "Simulation", priority=10)
for bb in [OUTPUTS_DIR / "simulation" / f"{ref}_reads.bb" for ref in runs.plasmids]:
    hub.add_track(f"{bb.basename().stripext()}_simulation", bb, "bigBed", color="red", group="sim")
```

```python
hub.add_group("CDS", "CDS", priority=2)
for bb in [OUTPUTS_DIR / "CDS" / f"{ref}.bb" for ref in chain(runs.genomes, runs.plasmids)]:
    hub.add_track(bb.basename().stripext(), bb, "bigBed", visibility="pack", color="blue", group="CDS")
```

```python
hub.add_group("reads", "reads", priority=3)
for sample_name, row in runs.samples.iterrows():
    bw = OUTPUTS_DIR / f"{row.reads_name}.bw"
    hub.add_track(row.reads_name, bw, "bigWig", visibility="full", group="reads")
```

```python
# upload it
hub.upload("localhost", OUTPUTS_DIR / "trackhub")
hub.upload("pataclet", "/mnt/data/trackhub/200131")
```

```python

```
